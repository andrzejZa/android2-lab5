package pl.edu.pwr.wiz.wzorlaboratorium5;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;

import javax.net.ssl.HttpsURLConnection;

import de.greenrobot.event.EventBus;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    DrawerLayout mDrawerLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

         mDrawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        processFilterSelection(menuItem);
                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        return true;
                    }
                });

        fetchData();
        ListView lv = findViewById(R.id.posts_list);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Post entry= (Post) parent.getAdapter().getItem(position);
                Intent intent = new Intent(MainActivity.this, CommentsActivity.class);
                if (entry !=null) {
                    intent.putExtra("postId", entry.id);
                    intent.putExtra("postTitle", entry.title);
                    intent.putExtra("postBody", entry.body);
                    intent.putExtra("userId", entry.userId);
                    startActivity(intent);
                }
            }
        });
        initDrawer();
    }


    void initDrawer(){
        NavigationView navigationView = findViewById(R.id.nav_view);
        PostFilter filter = PostFilter.Get();
        if (filter.option == FilterOptions.user ){
            navigationView.getMenu().findItem(R.id.filter_user_remove).setVisible(true);

        } else if (filter.option == FilterOptions.post ){
            navigationView.getMenu().findItem(R.id.filter_id_remove).setVisible(true);

        } else {
            navigationView.getMenu().findItem(R.id.filter_user_remove).setVisible(false);
            navigationView.getMenu().findItem(R.id.filter_id_remove).setVisible(false);
            PostFilter.Get().clear();

        }
    }

    void processFilterSelection(MenuItem item){
        NavigationView navigationView = findViewById(R.id.nav_view);
        if (item.getItemId() == R.id.filter_user ){
            PostFilter.Get().option = FilterOptions.user;
            navigationView.getMenu().findItem(R.id.filter_user_remove).setVisible(true);
            showDialog("Enter user ID");
        } else if (item.getItemId() == R.id.filter_id ){
            PostFilter.Get().option = FilterOptions.post;
            navigationView.getMenu().findItem(R.id.filter_id_remove).setVisible(true);
            showDialog("Enter post ID");
        } else {
            navigationView.getMenu().findItem(R.id.filter_user_remove).setVisible(false);
            navigationView.getMenu().findItem(R.id.filter_id_remove).setVisible(false);
            PostFilter.Get().clear();
            fetchData();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_retrofit :     startActivity(new Intent(this, RetrofitActivity.class));
                                            break;
            case R.id.action_webkit :       startActivity(new Intent(this, WebviewActivity.class));
                                            break;
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    /* Obsluga powrotu z wątku z danymi */
    public void onEventMainThread(PostLoadedEvent event) {
        ListView listView = (ListView) findViewById(R.id.posts_list);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressbar);

        listView.setAdapter(new PostsAdapter(getApplicationContext(), event.getPosts()));

        progressBar.setVisibility(View.INVISIBLE);
        listView.setVisibility(View.VISIBLE);
    }

    /* Funkcja pobiera dane z API */
    private boolean fetchData() {
        /* Ustawiamy widoczność progress baru i chowamy listę */
        ListView listView = (ListView) findViewById(R.id.posts_list);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressbar);

        listView.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);

        /* Uruchamiamy wątek do ładowania danych przy pomocy HttpsUrlConnection */
        new LoadThread().start();

        return true;
    }
    void showDialog(String text){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(text);

        final EditText input = new EditText(MainActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alert.setView(input);

//        EditText editText = (EditText) dialogView.findViewById(R.id.label_field);
//        editText.setText("test label");

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                if (PostFilter.Get().option == FilterOptions.user ){
                    PostFilter.Get().UserId = input.getText().toString();
                }
                if (PostFilter.Get().option == FilterOptions.post ){
                    PostFilter.Get().PostId = input.getText().toString();
                }
                fetchData();
                //Put actions for OK button here
                //filter(input.getText().toString(),alertType);
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Put actions for CANCEL button here, or leave in blank
            }
        });
        AlertDialog alertDialog = alert.create();
        alert.show();
    }

    /* Wątek do pobierania danych w tle */
    class LoadThread extends Thread {

        @Override
        public void run() {
            try {
                HttpsURLConnection c =
                        (HttpsURLConnection ) new URL(PostFilter.Get().getUrl()).openConnection();
                try {
                    InputStream in = c.getInputStream();
                    BufferedReader reader =
                            new BufferedReader(new InputStreamReader(in));
                    Post[] posts;
                    if (PostFilter.Get().option != FilterOptions.post) {
                         posts =
                                new Gson().fromJson(reader, Post[].class);
                    } else {
                        Post post =  new Gson().fromJson(reader, Post.class);
                        posts = new Post[]{post};
                    }
                    reader.close();

                    /* Wywołujemy event */
                    EventBus.getDefault().post(new PostLoadedEvent(posts));

                } catch (IOException e) {
                    Log.e(getClass().getSimpleName(), "Błąd parsowania formatu JSON", e);
                } finally {
                    c.disconnect();
                }
            }
            catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Błąd parsowania formatu JSON", e);
            }
        }
    }
}
