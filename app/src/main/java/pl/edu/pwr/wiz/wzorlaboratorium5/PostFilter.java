package pl.edu.pwr.wiz.wzorlaboratorium5;

public class PostFilter {
    String POSTS_URL = "https://jsonplaceholder.typicode.com/posts";
    private static PostFilter instance;
    public static PostFilter Get(){
        if (instance == null)
            instance = new PostFilter();
        return instance;
    }
    FilterOptions option;
    String UserId;
    String PostId;
    public void clear(){
        instance = new PostFilter();
    }

    public String getUrl(){
        if (option == FilterOptions.user && UserId != null && UserId.trim() !=""){
            return POSTS_URL+"?userId="+UserId;
        }
        if (option == FilterOptions.post && PostId != null && PostId.trim() !=""){
            return POSTS_URL+"/"+PostId;
        }
        return  POSTS_URL;
    }
}

enum FilterOptions{
    none,
    user,
    post
        }