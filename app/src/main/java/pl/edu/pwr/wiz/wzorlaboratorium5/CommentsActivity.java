package pl.edu.pwr.wiz.wzorlaboratorium5;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import de.greenrobot.event.EventBus;

public class CommentsActivity extends AppCompatActivity {
     Integer id;
     Integer userId;
     String title;
     String body;
     Comment[] comments;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

         id= getIntent().getIntExtra("postId",0);
        userId= getIntent().getIntExtra("userId",0);
         title = getIntent().getStringExtra("postTitle");
         body = getIntent().getStringExtra("postBody");
        final EditText postTitle = findViewById(R.id.post_title);
        postTitle.setText(title);
        final EditText postBody = findViewById(R.id.post_body);
        postBody.setText(body);
        final Button editBtn = findViewById(R.id.edit_btn);
        postTitle.setFocusable(false);
        postBody.setFocusable(false);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editBtn.getTag().toString().equals("edit")){
                    editBtn.setTag("save");
                    editBtn.setText("Save");
                    postTitle.setFocusableInTouchMode(true);
                    postTitle.setFocusable(true);
                    postTitle.setEnabled(true);
                    postBody.setFocusableInTouchMode(true);
                    postBody.setFocusable(true);
                    postBody.setEnabled(true);


                } else if (editBtn.getTag().toString().equals("save")){
                    editBtn.setTag("edit");
                    editBtn.setText("Edit");
                    postTitle.setEnabled(false);
                    postBody.setEnabled(false);
                    postTitle.setFocusable(false);
                    postBody.setFocusable(false);
                    Post editedPost =  new Post(userId,id,title,body);
                    new EditPostThread().setPost(userId,id,title,body).start();

                }
            }
        });
        final Button addComment = findViewById(R.id.add_comment);
        addComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog("Add new comment");
            }
        });
        loadComments();
        ListView listView = (ListView) findViewById(R.id.comment_list);
        addComment.requestFocus();
        //hideKeyboard(this);

    }

    void loadComments(){
        ListView listView = (ListView) findViewById(R.id.comment_list);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressbar_comments);

        progressBar.setVisibility(View.VISIBLE);
        listView.setVisibility(View.INVISIBLE);
        new CommentsActivity.LoadThread().start();
    }


    void showDialog(String text){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(text);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.add_comment, null);
        alert.setView(dialogView);

//        EditText editText = (EditText) dialogView.findViewById(R.id.label_field);
//        editText.setText("test label");

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                EditText nameNew = (EditText) dialogView.findViewById(R.id.new_com_title);
                EditText emailNew = (EditText) dialogView.findViewById(R.id.new_com_email);
                EditText bodyNew = (EditText) dialogView.findViewById(R.id.new_com_body);
                Comment comment = new Comment(id,
                        nameNew.getText().toString(),
                        emailNew.getText().toString(),
                        bodyNew.getText().toString());
                ListView listView = (ListView) findViewById(R.id.comment_list);
                List<Comment> new_comment = new ArrayList<Comment>();
                new_comment.addAll(Arrays.asList(comments));
                new_comment.add(comment);
                Comment[] new_comments_array = new Comment[new_comment.size()];
                comments = new_comment.toArray(new_comments_array);
                //((CommentAdapter)listView.getAdapter()).notifyDataSetChanged();
                listView.setAdapter(new CommentAdapter(getApplicationContext(), comments));
            hideKeyboard(CommentsActivity.this);


                new CommentsActivity.WriteCommentThread().setComment(comment).start();
                //Put actions for OK button here
                //filter(input.getText().toString(),alertType);
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Put actions for CANCEL button here, or leave in blank
            }
        });
        AlertDialog alertDialog = alert.create();
        alert.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    public void onEventMainThread(CommentsLoadedEvent event) {
        ListView listView = (ListView) findViewById(R.id.comment_list);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressbar_comments);
        comments = event.getComments();
        listView.setAdapter(new CommentAdapter(getApplicationContext(), comments));
        progressBar.setVisibility(View.INVISIBLE);
        listView.setVisibility(View.VISIBLE);
    }

    public void onEventMainThread(CommentAddedEvent event) {
        Toast.makeText(getApplicationContext(),event.getResponce(),Toast.LENGTH_LONG).show();
    }

    public void onEventMainThread(PostUpdatedEvent event) {
        Toast.makeText(getApplicationContext(),event.getResponce(),Toast.LENGTH_LONG).show();
    }


    public static void hideKeyboard(Activity activity) {
        try {
            View view = activity.getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    class LoadThread extends Thread {
  String URL = "https://jsonplaceholder.typicode.com/posts/"+id+"/comments";
        @Override
        public void run() {
            try {
                HttpsURLConnection c =
                        (HttpsURLConnection ) new URL(URL).openConnection();
                try {
                    InputStream in = c.getInputStream();
                    BufferedReader reader =
                            new BufferedReader(new InputStreamReader(in));
                    Comment[] posts =
                            new Gson().fromJson(reader, Comment[].class);
                    reader.close();

                    /* Wywołujemy event */
                    EventBus.getDefault().post(new CommentsLoadedEvent(posts));

                } catch (IOException e) {
                    Log.e(getClass().getSimpleName(), "Błąd parsowania formatu JSON", e);
                } finally {
                    c.disconnect();
                }
            }
            catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Błąd parsowania formatu JSON", e);
            }
        }
    }


    class WriteCommentThread extends Thread {
        Comment comment;
        public WriteCommentThread setComment(Comment comment){
            this.comment = comment;
            return this;
        }
        String URL = "https://jsonplaceholder.typicode.com/posts/"+id+"/comments";
        @Override
        public void run() {
            try {
                HttpsURLConnection c =
                        (HttpsURLConnection ) new URL(URL).openConnection();
                c.setRequestMethod("POST");
                c.setRequestProperty("Content-Type", "application/json");
                try {
                    OutputStream out = c.getOutputStream();

                    out.write(new Gson().toJson(comment).getBytes());
                    out.close();
                    InputStream in = c.getInputStream();
                    String temp = ((HttpsURLConnection)c).getResponseMessage();


                    /* Wywołujemy event */
                    EventBus.getDefault().post(new CommentAddedEvent(temp));

                } catch (IOException e) {
                    Log.e(getClass().getSimpleName(), "Błąd zapisywania kommentarzu", e);
                } finally {
                    c.disconnect();
                }
            }
            catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Błąd parsowania formatu JSON", e);
            }
        }
    }

    class EditPostThread extends Thread {
        Post post;

        public EditPostThread setPost(Integer userId, Integer id, String title, String body){
            post = new Post(userId,id,title,body);
            return this;
        }
        String URL = "https://jsonplaceholder.typicode.com/posts/"+id;
        @Override
        public void run() {
            try {
                HttpsURLConnection c =
                        (HttpsURLConnection ) new URL(URL).openConnection();
                c.setRequestMethod("PUT");
                c.setRequestProperty("Content-Type", "application/json");
                try {
                    OutputStream out = c.getOutputStream();

                    out.write(new Gson().toJson(post).getBytes());
                    out.close();
                    InputStream in = c.getInputStream();
                    String temp = ((HttpsURLConnection)c).getResponseMessage();


                    /* Wywołujemy event */
                    EventBus.getDefault().post(new PostUpdatedEvent(temp));

                } catch (IOException e) {
                    Log.e(getClass().getSimpleName(), "Błąd zapisywania kommentarzu", e);
                    EventBus.getDefault().post(new PostUpdatedEvent("Błąd zapisywania kommentarzu\""));
                } finally {
                    c.disconnect();
                }
            }
            catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Błąd parsowania formatu JSON", e);
            }
        }
    }
}
