package pl.edu.pwr.wiz.wzorlaboratorium5;

/**
 * Prosta klasa do przechowywania danych o wpisach
 */

public class Post {
    Integer userId, id;
    String title, body;

    public Post(Integer userId, Integer id, String title, String body) {
        this.userId = userId;
        this.id = id;
        this.title = title;
        this.body = body;
    }
}
