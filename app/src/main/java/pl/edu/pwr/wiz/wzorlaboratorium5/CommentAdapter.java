package pl.edu.pwr.wiz.wzorlaboratorium5;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class CommentAdapter extends ArrayAdapter<Comment> {
    Context mcontext;

    CommentAdapter(Context context, Comment[] comments) {
        super(context, android.R.layout.simple_list_item_1, comments);

        mcontext = context;
    }

    CommentAdapter(Context context, List<Comment> comments) {
        super(context, android.R.layout.simple_list_item_1, comments);

        mcontext = context;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.comment_info, parent, false);

        TextView id = (TextView) row.findViewById(R.id.comment_id);
        if (getItem(position).id != null) {
            id.setText(Integer.toString(getItem(position).id));
        }else {
    id.setText("");}


        TextView title = (TextView) row.findViewById(R.id.comment_title);
        title.setText(getItem(position).name);

        TextView email = (TextView) row.findViewById(R.id.email_txt);
        email.setText(getItem(position).email);

        TextView body = (TextView) row.findViewById(R.id.comment_text);
        body.setText(getItem(position).body);

        return(row);
    }
}
