package pl.edu.pwr.wiz.wzorlaboratorium5;

public class CommentsLoadedEvent {
    Comment[] comments;

    public CommentsLoadedEvent(Comment[] comments) {
        this.comments = comments;
    }

    public Comment[] getComments() {
        return comments;
    }
}
